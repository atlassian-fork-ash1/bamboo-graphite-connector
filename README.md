# bamboo-graphite-connector

bamboo-graphite-connector is a plugin for [Bamboo CI](https://www.atlassian.com/software/bamboo/).
It provides APIs to send statistics of bamboo to a [graphite](http://graphite.wikidot.com/) server.

## Configure graphite server connection information on bamboo

 1. Install the plugin and navigate to http://BAMBOO_INSTANCE/CONTEXT_PATH/admin/configureGraphiteConnector.action
 2. Uncheck "Disable sending data to Graphite" and specify graphite server and port. 
 3. Save your configuration.

## Send data to graphite.

After configuring the connection, you can

### Import `GraphiteConnector` component in your plugin's `atlassian-plugin.xml`.
    
```
<component-import key="graphiteConnector" name="Using Graphite Connector Service">
    <interface>com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnector</interface>
</component-import>
```

### Inject `GraphiteConnector` into your bean and begin to send data.

```
public class MyClass 
{
    private final GraphiteConnector graphiteConnector;
    
    public GraphiteServiceImpl(GraphiteConnector graphiteConnector)
    {
        this.graphiteConnector = graphiteConnector;
    }

    public void reportToGraphite() {
        logger.info("Sending the current metrics to Graphite.");
        GraphiteConnectorBulkSender bulkSender = graphiteConnector.getBulkSender();

        bulkSender.write(PATH_IN_GRAPHITE, "key1", "value1", timestamp);
        bulkSender.write(PATH_IN_GRAPHITE, "key2", "value2", timestamp);

        int sentCount = bulkSender.send();
        // Alternatively you can use following code to send data one by one which is quite
        // ineffecient by causing network traffic for every single piece of data
        // graphiteConnector.write(PATH_IN_GRAPHITE, "key1", "value1", timestamp);
        // graphiteConnector.write(PATH_IN_GRAPHITE, "key1", "value1", timestamp);

        logger.debug("Successfully sent {} metrics to Graphite.", sentCount);
    }
}
```
