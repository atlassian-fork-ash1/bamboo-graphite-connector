package ut.com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plugin.graphiteconnector.DefaultGraphiteConnectorConfiguration;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorConfiguration;
import com.atlassian.bamboo.plugin.graphiteconnector.GraphiteConnectorConstants;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bandana.BandanaManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: dkovacs
 * Date: 18/06/13
 * Time: 3:15 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class GraphiteConnectorConfigurationTest
{

    @Mock
    private AdministrationConfigurationAccessor administrationConfigurationManager;

    @Mock
    private BambooAuthenticationContext bambooAuthenticationContext;

    @Mock
    private BambooPermissionManager bambooPermissionManager;

    @Mock
    private BandanaManager bandanaManager;


    @Before
    public void setup() throws IOException
    {
        AdministrationConfiguration administrativeConfiguration = mock(AdministrationConfiguration.class);
        when(administrativeConfiguration.getBaseUrl()).thenReturn("http://myhost.atlassian.com");
        when(administrationConfigurationManager.getAdministrationConfiguration()).thenReturn(administrativeConfiguration);
        //when(bandanaManager.getValue()).thenReturn(null);
        //when(bambooAuthenticationContext.getUser()).thenReturn( ??????? );
        //when(bambooPermissionManager.isAdmin()).thenReturn(true);
    }

    @Test
    public void TestLoadingInitializingSettingAndStoringAttributes()
    {
        final boolean expectedGraphiteSendingEnabled = true;
        final String expectedGraphiteHost = "localhost";
        final int expectedGraphitePort = 9999;
        final String expectedPathPattern = "%s.servers.%s.%s";
        final String expectedSourceHostName = "myhost_atlassian_com";

        final GraphiteConnectorConfiguration configuration = new DefaultGraphiteConnectorConfiguration(administrationConfigurationManager, bambooAuthenticationContext, bambooPermissionManager, bandanaManager );

        when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED )).thenReturn( expectedGraphiteSendingEnabled );
        when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME )).thenReturn( expectedGraphiteHost );
        when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT )).thenReturn( expectedGraphitePort );
        //when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_PATH_PATTERN )).thenReturn( expectedPathPattern );

        assertEquals("GraphiteSendingEnabled does not match expected.", configuration.getGraphiteSendingEnabled(), expectedGraphiteSendingEnabled );
        assertEquals("GraphiteHost does not match expected.", configuration.getGraphiteHostName(), expectedGraphiteHost);
        assertEquals("GraphitePort does not match expected.", (long)configuration.getGraphitePort(), expectedGraphitePort);
        assertEquals("SourceHostName does not match expected.", configuration.getSourceHostname(), expectedSourceHostName);

        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED );
        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME );
        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT );

        configuration.setGraphiteSendingEnabled(expectedGraphiteSendingEnabled);
        configuration.setGraphiteHostName(expectedGraphiteHost);
        configuration.setGraphitePort(expectedGraphitePort);

        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED, expectedGraphiteSendingEnabled);
        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME, expectedGraphiteHost);
        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT, expectedGraphitePort);
    }


    @Test
    public void TestOpeningCorrectSocket() throws IOException
    {
        final String expectedHost = "localhost";
        final int expectedPort = 9999;

        ServerSocket listener = new ServerSocket(expectedPort);
        try
        {
            final GraphiteConnectorConfiguration configuration = new DefaultGraphiteConnectorConfiguration(administrationConfigurationManager, bambooAuthenticationContext, bambooPermissionManager, bandanaManager );

            when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME )).thenReturn( expectedHost );
            when(bandanaManager.getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT )).thenReturn( expectedPort );

            Socket socket = configuration.getSocket();

            try
            {
                final InetSocketAddress remoteSocketAddress = (InetSocketAddress) socket.getRemoteSocketAddress();

                assertEquals( "Opened socket hostname does not match given host", remoteSocketAddress.getHostName(), "localhost" );
                assertEquals( "Opened socket port does not match given host", remoteSocketAddress.getPort(), expectedPort );
            }

            finally
            {
                socket.close();
            }
        }
        finally
        {
            listener.close();
        }

    }




//
//  NOTE: This test is intended to test the caching version of the GraphiteConnectorConfiguration, and
//  since caching is disabled, it is disabled as well.
//
//    @Test
//    public void TestLoadingInitializingSettingAndStoringAttributes()
//    {
//        final boolean expectedGraphiteSendingEnabled = true;
//        final String expectedGraphiteHost = "localhost";
//        final int expectedGraphitePort = 9999;
//        final String expectedPathPattern = "%s.servers.%s.%s";
//        final String expectedSourceHostName = "myhost_atlassian_com";
//
//        final GraphiteConnectorConfiguration configuration = new DefaultGraphiteConnectorConfiguration(administrationConfigurationManager, bambooAuthenticationContext, bambooPermissionManager, bandanaManager );
//
//        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED );
//        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME );
//        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT );
//        verify(bandanaManager).getValue( PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_PATH_PATTERN );
//
//        configuration.setGraphiteHostName(expectedGraphiteHost);
//        configuration.setGraphitePort(expectedGraphitePort);
//        configuration.setPathPattern(expectedPathPattern);
//
//        assertEquals("GraphiteHost does not match expected.", configuration.getGraphiteHostName(), expectedGraphiteHost);
//        assertEquals("GraphitePort does not match expected.", (long)configuration.getGraphitePort(), expectedGraphitePort);
//        assertEquals("PathPattern does not match expected.", configuration.getPathPattern(), expectedPathPattern);
//        assertEquals("SourceHostName does not match expected.", configuration.getSourceHostname(), expectedSourceHostName);
//
//        configuration.store();
//
//        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_SENDING_ENABLED, expectedGraphiteSendingEnabled);
//        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_HOST_NAME, expectedGraphiteHost);
//        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_GRAPHITE_PORT, expectedGraphitePort);
//        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, GraphiteConnectorConstants.BANDANA_KEY_PATH_PATTERN, expectedPathPattern);
//    }


}
