<html>
<head>
    <meta name="decorator" content="atl.admin">
    <title>Configure Graphite Connector Plugin</title>
</head>
<body>
[@ww.form action="configureGraphiteConnector!edit.action" method="post" class="aui"]
    [@ui.bambooSection titleKey='Configure Graphite Connector Plugin' ]
    [@ww.checkbox label="Disable sending data to Graphite" name="graphiteSendingDisabled"/]
    [@ww.textfield labelKey='Graphite Host' name="graphiteHostName"]
        [@ww.param name='description']
            [@ww.text name='The host name of Graphite Server' /]
        [/@ww.param]
    [/@ww.textfield]
    [@ww.textfield labelKey='Graphite Port' name="graphitePort"]
        [@ww.param name='description']
            [@ww.text name='The port on the Graphite Server' /]
        [/@ww.param]
    [/@ww.textfield]
    [@ww.submit value='Update Configuration' theme="simple" cssClass='aui-button'/]
[/@ui.bambooSection]
[/@ww.form]
</body>
</html>