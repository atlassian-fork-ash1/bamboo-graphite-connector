package com.atlassian.bamboo.plugin.graphiteconnector;

/**
 * Message Queue allowing users to send multiple messages at once to Graphite.
 */

public interface GraphiteConnectorBulkSender extends GraphiteConnectorSender {

    /**
     * Send queued messages.
     *
     * @return Number of messages sent.
     */
    int send();

}
