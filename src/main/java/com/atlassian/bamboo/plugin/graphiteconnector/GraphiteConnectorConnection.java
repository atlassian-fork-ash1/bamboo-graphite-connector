package com.atlassian.bamboo.plugin.graphiteconnector;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Connection Interface is being used internally by GraphiteConnector
 */
public interface GraphiteConnectorConnection {

    int writeMessageList(@NotNull List<String> messages);

}
