package com.atlassian.bamboo.plugin.graphiteconnector;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the BulkSender Message Queue
 *
 * This class will cache up the write() calls, and send all the messages at once when send() is called.
 *
 */
class DefaultGraphiteConnectorBulkSender implements GraphiteConnectorBulkSender {

    private final GraphiteConnectorConfiguration configuration;
    private final GraphiteConnectorConnection connection;
    private final List<String> messages = new ArrayList<String>();


    public DefaultGraphiteConnectorBulkSender(@NotNull GraphiteConnectorConfiguration configuration,
                                              @NotNull GraphiteConnectorConnection graphiteConnectorConnection) {
        this.configuration = configuration;
        this.connection    = graphiteConnectorConnection;
    }

    @Override
    public int send() {
        this.connection.writeMessageList(messages);
        int sentCount = messages.size();
        messages.clear();
        return sentCount;
    }

    @Override
    public void write(String path, String label, String value) {
        this.write( path, label, value, System.currentTimeMillis() / 1000L );
    }

    @Override
    public void write(String path, String label, String value, long timestamp) {
        messages.add( GraphiteConnectorConstants.formatGraphiteMessage( configuration, path, label, value, timestamp ) );
    }
}

