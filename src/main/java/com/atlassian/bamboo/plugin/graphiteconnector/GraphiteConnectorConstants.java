package com.atlassian.bamboo.plugin.graphiteconnector;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that holds constants and static methods used by GraphiteConnector
 */
public class GraphiteConnectorConstants {

    // Bandana Keys and Default Values
    public static final String BANDANA_KEY_GRAPHITE_SENDING_ENABLED = "plugin.monitoring.graphiteConnector.graphiteSendingEnabled";
    public static final Boolean DEFAULT_GRAPHITE_SENDING_ENABLED = true;

    public static final String BANDANA_KEY_GRAPHITE_HOST_NAME = "plugin.monitoring.graphiteConnector.graphiteHost";
    public static final String DEFAULT_GRAPHITE_HOST_NAME = "graphite.buildeng.atlassian.com";


    public static final String BANDANA_KEY_GRAPHITE_PORT = "plugin.monitoring.graphiteConnector.graphitePort";
    public static final int DEFAULT_GRAPHITE_PORT = 2003;

    public static final String BANDANA_KEY_PATH_PATTERN = "plugin.monitoring.graphiteConnector.pathPattern";
    public static final String DEFAULT_PATH_PATTERN = "servers.%s.bamboo.%s.%s";


    public static final int SOCKET_TIMEOUT = 30000;

    //Helpers
    public static final Map<String, String> specialGraphiteValues = new HashMap<String, String>(){ { put("true", "1"); put("false", "0"); } };

    /**
     * Returns a graphite-compatible version of value.
     *
     * @param value Value to be sanitized
     * @return sanitized version of value
     */
    public static String sanitizeGraphiteValue(@NotNull String value) {
        if (specialGraphiteValues.containsKey(value)) {
            return specialGraphiteValues.get(value);
        }
        return value;
    }

    public static String sanitizeGraphitePath(@NotNull final GraphiteConnectorConfiguration configuration, @NotNull String path, @NotNull String label ) {
        String fullPath = String.format( configuration.getPathPattern(), configuration.getSourceHostname(), path, label );
        fullPath = fullPath.replace( "..", "." );
        if ( fullPath.endsWith(".") ) {
            fullPath = fullPath.substring( 0, fullPath.length() - 1 );
        }
        return fullPath;
    }


    /**
     * Returns the graphite-compatible formatting of the given HostName
     *
     * @param hostName Host name or URL in the form of &lt;protocol&gt;://&lt;hostname&gt;/&lt;url&gt;
     * @return sanitized version of host name
     */

    public static String sanitizeGraphiteHostName(String hostName) {
        return hostName.replaceAll("https://", "")
                       .replaceAll("http://", "")
                       .replaceAll("\\.", "_")
                       .replaceAll(":", "")
                       .replaceAll("/", "");
    }

    /**
     * Returns a message complying the graphite plaintext protocol
     *
     * GraphitePlaintextProtocol: http://graphite.readthedocs.org/en/1.0/feeding-carbon.html
     *
     * @param configuration Instance of the configuration class
     * @param path Graphite path of the measurement to be displayed in. Elements must be separated by dots.
     * @param label Label of the measurement
     * @param value Value of the measurement
     * @param timestamp Timestamp of the measurement
     * @return Graphite plain text protocol formatted message.     */

    public static String formatGraphiteMessage(@NotNull GraphiteConnectorConfiguration configuration, String path, String label, String value, long timestamp) {
        return String.format("%s %s %s",
                sanitizeGraphitePath(configuration, path, label),
                sanitizeGraphiteValue(value),
                timestamp);
    }
}
